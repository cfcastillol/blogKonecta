<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$verifyLink = Yii::$app->urlManager->createAbsoluteUrl(['site/verify-email', 'token' => $user->verification_token]);
?>
Hola <?= $user->name ?>,

Da click en el siguiente enlace para Verificar tu cuenta:

<?= $verifyLink ?>
