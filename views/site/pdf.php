<?php
setlocale(LC_TIME, 'es_CO.UTF-8');
$formatter = \Yii::$app->formatter;
use yii\helpers\Html;
?>


<h1><?= $articulo->titulo ?></h1>

<p class='data-author'>
    Publicado por: <strong><?= $articulo->usuarioCrea->name ?></strong>
    el día <strong><?= $formatter->asDatetime($articulo->fecha_crea); ?></strong>
</p>

<?= $articulo->detalle ?>