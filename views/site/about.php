<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use app\models\Helper;
use yeesoft\lightbox\Lightbox;

/* @var $this yii\web\View */
$this->title = 'Acerca';
$this->params['breadcrumbs'][] = $this->title;
// parámetros para el sidebar
$this->params['categorias'] = $categorias;
$this->params['mas_visitados'] = $mas_visitados;
?>

<?php
$script = <<< JS
JS;
$this->registerJs($script);
?>

<div class="col-md-12">
    
    <h1 class="text-center"><?= Html::encode($this->title) ?></h1>
    
    <div class="d-flex justify-content-center text-center">
        <figure>

                <?= Lightbox::widget([
                    'options' => [
                        'fadeDuration' => '2000',
    //                    'albumLabel' => "Image %1 of %2",
                    ],
                    'linkOptions' => ['class' => 'btn'],
                    'imageOptions' => [
                        'class' => 'text-center thumbnail',
                        'alt' => 'Cristian Fabian Castillo Lopez',
                    ],
                    'items' => [
                        [
                            'thumb' => '@web/img/foto_thumbnail.jpeg',
                            'image' => '@web/img/foto.jpeg',
                            'title' => 'Cristian Fabian Castillo Lopez',
    //                        'group' => 'image-set1',
                        ],
                    ],
                ]);
                ?>
            <figcaption>
                
                <div class="vcard">
                    <div class="fn n">
                        <span class="given-name">Cristian Fabian</span> <span class="family-name">Castillo Lopez</span><br />
                        <!-- <span class="org">El Ejemplo S. A.</span> -->
                    </div>
                    <div>
                        <span class="nickname">cfcastillol</span>
                    </div>
                    <div class="adr">
                    <!-- <span class="street-address">Calle falsa 1</span><br /> -->
                        <span class="locality" title="La Dorada">La Dorada</span>,
                        <abbr class="region" title="Caldas">Caldas</abbr>,
                        <!-- <span class="postal-code">94301</span>, -->
                        <abbr class="country-name" title="Colombia">Colombia</abbr>
                    </div>
                    <!-- <li class="tel"><strong class="type" title="Teléfono del trabajo">Work</strong>: <span class="value">604-555-1234</span></li> -->
                        <!-- <li class="url"><strong class="type" title="Sitio web oficial del trabajo">Work</strong>: <a href="http://ejemplo.com/" title="Ejemplo.com" class="value">http://ejemplo.com/</a></li> -->
                </div>
                <span class="negrita">Edad</span>: <?= Helper::calculaEdad("1992-02-22"); ?> años
                <br />
                <span class="negrita">Ocupación</span>: Ingeniero sistemas - Desarrollador de Software
                
            </figcaption>
        </figure>
    </div>
        <h3>Software utilizado para el desarrollo</h3>
        <ul class="list-style-none">
            <li>
                <a href="https://netbeans.apache.org/" title="NetBeans IDE" target="_blank">
                    NetBeans IDE
                </a>
            </li>
            <li>
                <a href="http://www.mozilla.org/es-ES/firefox/fx/" title="Mozilla Firefox" target="_blank">
                    Mozilla Firefox
                </a>
            </li>
            <li>
                <a href="https://blonder413.blogspot.com/2020/05/instalar-lampp-en-ubuntu-2004.html" title="LAMP" target="_blank">
                    LAMPP
                </a>
            </li>
            <li>
                <a href="https://gitlab.com/cfcastillol" title="GIMP" target="_blank">
                    GitLab
                </a>
            </li>
            <li>
                <a href="https://getcomposer.org/" title="Composer" target="_blank">
                    Composer
                </a>
            </li>
            <li>
                <a href="https://www.yiiframework.com/" title="Composer" target="_blank">
                    Yii framework
                </a>
            </li>
            <li>
                <a href="https://dbeaver.io/" title="Composer" target="_blank">
                    DBeaver
                </a>
            </li>
        </ul>

        <hr>
        <div class="row">
            <div class="col col-xs-12 col-md-6">
                <!-- Place this tag in your head or just before your close body tag. -->
                <script src="https://apis.google.com/js/platform.js" async defer>
                    {
                        lang: 'es-419'
                    }
                </script>

                <!-- Place this tag where you want the widget to render. -->
                <div class="g-page" data-href="//plus.google.com/u/0/114140664117985392477" data-rel="publisher"></div>
            </div>
            <div class="col col-xs-12 col-md-6">
                <!-- Place this tag where you want the widget to render. -->
                <div class="g-community" data-href="https://plus.google.com/communities/104747714155968688815"></div>

                <!-- Place this tag after the last widget tag. -->
                <script type="text/javascript">
                    window.___gcfg = {lang: 'es-419'};
                    (function () {
                        var po = document.createElement('script');
                        po.type = 'text/javascript';
                        po.async = true;
                        po.src = 'https://apis.google.com/js/platform.js';
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(po, s);
                    })();
                </script>
            </div>
        </div>
        <hr>
    
        <iframe width="700" height="500" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/?ie=UTF8&amp;t=h&amp;ll=5.459905,-74.660339&amp;spn=0.041012,0.054932&amp;z=14&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com/?ie=UTF8&amp;t=h&amp;ll=5.459905,-74.660339&amp;spn=0.041012,0.054932&amp;z=14&amp;source=embed" style="color:#0000FF;text-align:left" target='_blank'>Ver mapa más grande</a></small>
    
</div>