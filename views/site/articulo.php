<?php

use app\models\Helper;
use kartik\growl\Growl;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\bootstrap4\Breadcrumbs;

/* @var $this yii\web\View */

// Pone la fecha en español
//setlocale(LC_TIME, 'es_CO.UTF-8');

$this->title = "BlogKonecta | $articulo->titulo";


$this->params['breadcrumbs'][] = [
    'label' => $articulo->categoria->categoria, 
    'url' => ['categoria/' . $articulo->categoria->slug]
];

$this->params['breadcrumbs'][] = $articulo->titulo;

// parámetros para el sidebar
$this->params['categorias'] = $categorias;

$formatter = \Yii::$app->formatter;
?>

<article class="blog-post">
    <h2 class="blog-post-title"><?= $articulo->titulo; ?></h2>
    <p class="blog-post-meta">
        <?=
        Html::a(
                ucwords($articulo->usuarioCrea->name),
                ['autor/' . urlencode($articulo->usuarioCrea->name)],
                [
                    'rel' => 'author', 
                    'title' => 'Ver artículos del usuario ' . $articulo->usuarioCrea->name
                ]
        );
        ?> | 
        <?= $formatter->asDatetime($articulo->fecha_crea); ?> | 
        <?=
        Html::a(
                $articulo->categoria->categoria,
                ['categoria/' . urlencode($articulo->categoria->slug)],
                [
                    'title' => 'Ver artículos de la categoría ' . $articulo->categoria->categoria
                ]
        );
        ?> | 
        <?= $articulo->vistas; ?> visitas

        <?php if (\Yii::$app->user->can('articulu-admin') or \Yii::$app->user->can('articulo-actualizar', ['articulos' => $articulo])): ?>

            |

        <?=
        Html::a(
            '<i class="fas fa-pen"></i> Actualizar',
            ['articulos/update', 'id' => $articulo->id], 
            [
                'title' => 'Actualizar artículo',
            ]
        );
        ?>

    <?php endif; ?>

    </p>

                <?= $articulo->detalle; ?>

                <?php if (!empty($articulo->video)): ?>
        <div class="video-responsive text-center">
                    <?= $articulo->video; ?>
        </div>
                <?php endif; ?>

    <div class="col-sm-12">
        <p>
            <span class="glyphicon glyphicon-tags">&nbsp;Etiquetas:&nbsp;</span>
            <?php foreach ($etiquetas as $key => $value): ?>
                <span class="badge">
                <?= Html::a($value, ["etiqueta/$value"]) ?>
                </span>
            <?php endforeach; ?>
        </p>
    </div>

    <div class="col-md-12">
        <div class="col-md-3">
            <?php if (!empty($article->download)): ?>

                <?=
                Html::a(
                        "<span class='glyphicon glyphicon-floppy-save'></span> Descargar",
                        // $article->download,
                        'descarga/' . $article->slug,
                        [
                            "class" => "btn btn-primary btn",
                            // 'target'    => '_blank',
                            'title' => 'Descargar',
                        ]
                )
                ?>
            <?php endif; ?>
        </div>
        <div class="col-md-3">
            <?=
            Html::a(
                    "<i class='fas fa-file-pdf'></i>&nbsp;Exportar",
                    ['/pdf/' . $articulo->slug],
                    [
                        'class' => 'btn btn-primary btn',
                        'target' => '_blank',
                        'title' => 'Exportar a PDF',
                    ]
            )
            ?>
        </div>
    </div>

    <div class="comment-post">
        <?php
        if (Yii::$app->session->getFlash('success')) {
            echo Growl::widget([
                'type' => Growl::TYPE_SUCCESS,
                'title' => 'Comentario registrado!',
                'icon' => 'glyphicon glyphicon-ok-sign',
                'body' => Yii::$app->session->getFlash('success'),
                'showSeparator' => true,
                'delay' => 0, // time before display
                'pluginOptions' => [
                    'placement' => [
                        'from' => 'top',
                        'align' => 'center',
                    ],
                    'showProgressbar' => false,
                    'timer' => 3000, // screen time
                ]
            ]);
        } elseif (Yii::$app->session->getFlash('error')) {
            echo Growl::widget([
                'type' => Growl::TYPE_DANGER,
                'title' => 'Error al registrar el comentario!',
                'icon' => 'glyphicon glyphicon-ok-sign',
                'body' => Yii::$app->session->getFlash('error'),
                'showSeparator' => true,
                'delay' => 0, // time before display
                'pluginOptions' => [
                    'placement' => [
                        'from' => 'top',
                        'align' => 'center',
                    ],
                    'showProgressbar' => false,
                    'timer' => 3000, // screen time
                ]
            ]);
        } elseif (Yii::$app->session->getFlash('download_error')) {
            echo Growl::widget([
                'type' => Growl::TYPE_DANGER,
                'title' => 'Error de descarga!',
                'icon' => 'glyphicon glyphicon-ok-sign',
                'body' => Yii::$app->session->getFlash('download_error'),
                'showSeparator' => true,
                'delay' => 0, // time before display
                'pluginOptions' => [
                    'placement' => [
                        'from' => 'top',
                        'align' => 'center',
                    ],
                    'showProgressbar' => false,
                    'timer' => 3000, // screen time
                ]
            ]);
        }
        ?>

        <h2>Comentar</h2>

<?php
echo $this->render('/comentario/_form', [
    'model' => $comentario,
])
?>

        <a name="comments"></a>

        <h2>Comentarios</h2>

        <!-- listado de comentarios -->
        <ul class="item-comment">
                    <?php foreach ($articulo->comentarios as $key => $value): ?>
                        <?php if ($value->estado == 1): ?>
                    <li>
                        <span class="item-comment-name">
                           <i class="fas fa-user"></i>&nbsp;
                            <?php
                            if (empty($value->web)) {
                                echo Html::encode("{$value->nombre}");
                            } else {
                                echo Html::a(
                                        Html::encode("{$value->nombre}"),
                                        Html::encode("{$value->web}"),
                                        [
                                            'target' => '_blank',
                                            'rel' => $value->rel,
                                            'title' => Html::encode("{$value->web}"),
                                        ]
                                );
                            }
                            ?>
                        </span>
                        &nbsp;
                        <i class="far fa-calendar-alt"></i> <?= $formatter->asDatetime($value->fecha); ?>
                        <p><?= "<i class='fas fa-comment-alt'></i> " . Html::encode("{$value->comentario}"); ?></p>
                    </li>
    <?php endif; ?>
<?php endforeach; ?>
        </ul>
        <!-- fin listado de comentarios -->
    </div>

</article><!-- /.blog-post -->