<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>En Mantenimiento | BlogKonecta</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <meta http-equiv='refresh' content='60'>
  </head>
  <body>
    <div class="container">
      <div class='card'>
        <div class='card-header'>
          <h2 class='card-title'>Servicios en Mantenimiento</h2>
        </div>
        <div class='card-body'>
          <pre><code>
            root@cfcastillol-Konecta ~ # apt update && apt dist-upgrade
            Obj:1 https://twitter.com/cfcastillol/1 cfcastillol InRelease
            Obj:2 https://twitter.com/cfcastillol/2 cfcastillol InRelease
            Obj:3 https://twitter.com/cfcastillol/3 cfcastillol InRelease
            Obj:4 https://twitter.com/cfcastillol/4 cfcastillol InRelease
            Obj:5 https://twitter.com/cfcastillol/22 cfcastillol InRelease
            17521 paquetes para actualizar
            143.6 MB van a ser actualizados
            ¿Desea continuar? si / no
            root@cfcastillol-Konecta ~ # si
            Descargando paquetes
            ........................................................ 360 kb/s
          </code></pre>
        </div>
        <div class='card-footer text-center'>
          <small><em>
            Estaremos de vuelta en cuanto el proceso se haya completado.
            No es necesario actualizar la página, ésta lo hace automáticamente.
          </em></small>
        </div>
      </div>
    </div>
  </body>
</html>
<style>
body{
  background-color: #fff;
  color: #000;
  font-size: 20px;
}
</style>