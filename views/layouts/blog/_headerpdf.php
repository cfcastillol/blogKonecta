<?php
use yii\helpers\Html;
?>

<section class="bg-primary">
    <div class="container">
        <h1 class="title-blog">
            <?= Html::img('@web/img/logo.png', ['width' => '20']) ?>
            BlogKonecta
        </h1>
        <p class="text-center">Soluciones a medida para la mejor experiencia cliente BPO y Contact Center</p>
    </div>
</section>