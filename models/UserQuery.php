<?php
namespace app\models;
use yii\db\ActiveQuery;
class UserQuery extends ActiveQuery
{
    // conditions appended by default (can be skipped)
    public function init()
    {
        // $this->andOnCondition(['deleted' => false]);
        parent::init();
    }
    
    // ... add customized query methods here ...
    public function active($status = User::STATUS_ACTIVE)
    {
        return $this->andOnCondition(['user.status' => $status]);
    }
}