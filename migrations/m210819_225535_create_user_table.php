<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user}}`.
 */
class m210819_225535_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
   public function safeUp()
    {
        $tableOptions = null;
        
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_spanish_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'name'                  => $this->string()->notNull(),
            'auth_key'              => $this->string(32)->notNull(),
            'email'                 => $this->string()->notNull()->unique(),
            'photo'                 => $this->string()->notNull()->unique(),
            'celular'               => $this->string(20)->notNull(),
            'status'                => $this->smallInteger()->notNull()->defaultValue(10),
            'verification_token'    => $this->string()->defaultValue(null),
            'password_hash'         => $this->string()->notNull(),
            'password_reset_token'  => $this->string()->unique(),
            'created_at'            => $this->integer(),
            'updated_at'            => $this->integer(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
