<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%articulo}}`.
 */
class m210819_230501_create_articulo_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_spanish_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%articulo}}', [
            'id'                => $this->primaryKey(),
            'titulo'            => $this->string(150)->unique()->notNull(),
            'slug'              => $this->string(150)->unique()->notNull(),
            'resumen'           => $this->string(300)->notNull(),
            'detalle'           => $this->text()->notNull(),
            'categoria_id'      => $this->integer()->notNull(),
            'etiquetas'         => $this->string(255)->notNull(),
            'estado'            => $this->boolean()->notNull(),
            'vistas'            => $this->integer()->defaultValue(0)->notNull(),
            'usuario_crea'      => $this->integer()->notNull(),
            'fecha_crea'        => $this->dateTime()->notNull(),
            'usuario_modifica'  => $this->integer()->notNull(),
            'fecha_modifica'    => $this->dateTime()->notNull(),
        ], $tableOptions);
        
        $this->createIndex('idx-articulo-titulo', 'articulo', 'titulo');
        
        $this->addForeignKey(
            'categoriaarticulo', 'articulo', 'categoria_id', 'categoria', 'id', 'no action', 'no action'
        );
        
        $this->addForeignKey(
            'usuariocreaarticulo', 'articulo', 'usuario_crea', 'user', 'id', 'no action', 'no action'
        );
        
        $this->addForeignKey(
            'usuariomodificaearticulo', 'articulo', 'usuario_modifica', 'user', 'id', 'no action', 'no action'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('categoriaarticulo', 'articulo');
        $this->dropForeignKey('usuariocreaarticulo', 'articulo');
        $this->dropForeignKey('usuariomodificaarticulo', 'articulo');
        $this->dropTable('{{%articulo}}');
    }
}
