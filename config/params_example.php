<?php

return [
    'bsVersion' => '4.x',
    'bsDependencyEnabled' => false,
    'adminEmail' => 'micorreo@gmail.com',
    'supportEmail' => 'micorreo@gmail.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
];
