<?php
use kartik\mpdf\Pdf;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'konecta',
    'name' => 'BlogKonecta',
    'layout'    => 'blog/main',
    'language'  => 'es',
    'sourceLanguage'    => 'es-CO',
    'timeZone' => 'America/Bogota',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'cQ8DHirNE9dwmgJjI9wk3ZVMZSz8jn8D',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => require(__DIR__ . '/mailer.php'),
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                ['class' => 'yii\rest\UrlRule', 'controller' => 'articulo-rest'],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'apirest/articulo'],
                'articulo/<slug>'                   => 'site/articulo',
                'articulo/descarga/<slug>'          => 'site/descarga',
                'autor/<autor>/pagina/<page>'       => 'site/autor',
                'autor/<autor>'                     => 'site/autor',
                'categoria/<slug>/pagina/<page>'    => 'site/categoria',
                'categoria/<slug>'                  => 'site/categoria',
                'etiqueta/<etiqueta>/pagina/<page>' => 'site/etiqueta',
                'etiqueta/<etiqueta>'               => 'site/etiqueta',
                'acerca'                            => 'site/about',
                'inicio/pagina/<page>'              => 'site/index',
                'inicio'                            => 'site/index',
                'contacto'                          => 'site/contact',
                'login'                             => 'site/login',
                "registro"                          => "site/signup",
                'pdf/<slug>'                        => 'site/pdf',
                'portafolio'                        => 'site/portafolio',
                //'DELETE <controller:\w+>/<id:\d+>'  => '<controller>/delete',
            ],
        ],

        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest'],
//            'itemTable'         => 'auth_item',         // tabla para guardar objetos de autorización
//            'itemChildTable'    => 'auth_item_child',   // tabla para almacenar jerarquía elemento autorización
//            'assignmentTable'   => 'auth_assignment',   // tabla para almacenar las asignaciones de elementos de autorización
//            'ruleTable'         => 'auth_rule',         // tabla para almacenar reglas
        ],
        'formatter' => [
            'class' => '\yii\i18n\Formatter',
            'defaultTimeZone'   => 'America/Bogota',
//            'dateFormat' => 'dd.MM.yyyy',
            'datetimeFormat'    => 'php:D j \d\e M \d\e Y g:i a',
            'locale'    => 'es_ES',
            'language'  => 'es',
            'thousandSeparator' => '.',
            'decimalSeparator' => ',',
            'currencyCode' => '$'
        ],
        // https://demos.krajee.com/mpdf
        // setup Krajee Pdf component
        'pdf' => [
            'class'             => Pdf::classname(),
            'format'            => Pdf::FORMAT_LETTER,
            'mode'              => Pdf::MODE_CORE, 
            'orientation'       => Pdf::ORIENT_PORTRAIT,
            'destination'       => Pdf::DEST_BROWSER,
//            'cssFile'           => 'css/azul/pdf.css',
            'defaultFont'       => 'Roboto',
//            'marginTop'         => 13,
//            'marginBottom'      => 13,
//            'marginLeft'        => 13,
//            'marginRight'       => 13,
            'options'   => [
                'showWatermarkText' => true,
                'showWatermarkImage' => true,
            ],
            
            'defaultFontSize'   => 12,
            // refer settings section for all configuration options
        ],

    ],
    'modules' => [
        'gridview' => [
            'class' => '\kartik\grid\Module'
        // enter optional module parameters below - only if you need to  
        // use your own export download action or custom translation 
        // message source
        // 'downloadAction' => 'gridview/export/download',
        // 'i18n' => []
        ],
        'admin' => [
            'layout' => 'left-menu',
            'mainLayout' => '@app/views/layouts/adminLTE3/main.php',
            'class' => 'mdm\admin\Module',
            'controllerMap' => [
                'assignment' => [
                    'class' => 'mdm\admin\controllers\AssignmentController',
                    'userClassName' => 'app\models\User',
                    'idField' => 'id',
                    'usernameField' => 'username',
                    'fullnameField' => 'name',
//                    'extraColumns' => [
//                        [
//                            'attribute' => 'full_name',
//                            'label' => 'Full Name',
//                            'value' => function($model, $key, $index, $column) {
//                                return $model->profile->full_name;
//                            },
//                        ],
//                        [
//                            'attribute' => 'dept_name',
//                            'label' => 'Department',
//                            'value' => function($model, $key, $index, $column) {
//                                return $model->profile->dept->name;
//                            },
//                        ],
//                        [
//                            'attribute' => 'post_name',
//                            'label' => 'Post',
//                            'value' => function($model, $key, $index, $column) {
//                                return $model->profile->post->name;
//                            },
//                        ],
//                    ],
                    'searchClass' => 'app\models\UserSearch'
                ],
            ],
        ]
    ],
//    'as access' => [
//        'class' => 'mdm\admin\components\AccessControl',
//        'allowActions' => [
//            'site/index',
//            'site/logout',
//            'site/signup',
//            'site/request-password-reset',
//            'site/error',
//            'site/resend-verification-email',
//            'site/verify-email',
//            'site/reset-password',
//            'admin/*'
//        //'admin/*',
//        //'some-controller/some-action',
//        // The actions listed here will be allowed to everyone including guests.
//        // So, 'admin/*' should not appear here in the production, of course.
//        // But in the earlier stages of your development, you may probably want to
//        // add a lot of actions here until you finally completed setting up rbac,
//        // otherwise you may not even take a first step.
//        ]
//    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
            // uncomment the following to add your IP if you are not connecting from localhost.
            //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
            // uncomment the following to add your IP if you are not connecting from localhost.
            //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
