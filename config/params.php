<?php
return [
    'bsVersion' => '4.x',
    'bsDependencyEnabled' => false,
    'adminEmail' => 'cfcastillol@gmail.com',
    'supportEmail' => 'cfcastillol@gmail.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
];
