[![Gitter](https://badges.gitter.im/Yii-Framework-2/blog-yii-2.svg)](https://gitter.im/Yii-Framework-2/blog-yii-2?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

# Configuraciones iniciales del Sistema de información BlogKonecta

# Configurar conexión a la Base de Datos (db.php)

Asegúrate de tener instalado la extensión de PHP PDO y el driver de PDO para el motor que estés utilizando (ej. pdo_mysql para MySQL). 
Este es un requisito básico si tu aplicación va a utilizar bases de datos relacionales.

Abre el archivo config/db_example.php y ajusta el contenido dependiendo de la configuración a tu base de datos. Por defecto, el archivo contiene el siguiente contenido y luego
renombralo a db.php:

```
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
```
# Configurar envío de correos (mailer.php)
Abre el archivo config/mailer_example.php y ajusta el contenido dependiendo de la configuración de tu servicio de correos. Por defecto, el archivo contiene el siguiente contenido y luego
renombralo a mailer.php:

```
return [
    'class' => 'yii\swiftmailer\Mailer',
    'viewPath' => '@app/mail',
    // send all mails to a file by default. You have to set
    // 'useFileTransport' to false and configure a transport
    // for the mailer to send real emails.
    'useFileTransport' => false,
    'transport' => [
	'class' => 'Swift_SmtpTransport',
  	'host' => 'smtp.gmail.com',
//  	'host'  => 'smtp.mail.yahoo.com',
	//'host'      => 'smtp-mail.outlook.com',
		'username' => 'micorreo@gmail.com',
		'password' => 'mipasswd',
		'port' => '587',     // 25 o 465 para yahoo - 587 para GMail/outlook
		'encryption' => 'tls',
    ],
];
```

# Configurar archivo params.php 
Abre el archivo config/params_example.php y ajusta el contenido dependiendo de la configuración de tus variables. Por defecto, el archivo contiene el siguiente contenido y luego
renombralo a params.php cambias las varibales adminEmail, supportEmail, senderEmail, senderName:

```
return [
    'bsVersion' => '4.x',
    'bsDependencyEnabled' => false,
    'adminEmail' => 'micorreo@gmail.com',
    'supportEmail' => 'micorreo@gmail.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
];
```

# Manejo de esquema de Roles RBAC (Configuración)
Configura authManager en config\web.php para crear la migración de RBAC
```
'authManager'       => [
            'class'         => 'yii\rbac\DbManager',
            'defaultRoles'  => ['guest'],
//            'itemTable'         => 'auth_item',         // tabla para guardar objetos de autorización
//            'itemChildTable'    => 'auth_item_child',   // tabla para almacenar jerarquía elemento autorización
//            'assignmentTable'   => 'auth_assignment',   // tabla para almacenar las asignaciones de elementos de autorización
//            'ruleTable'         => 'auth_rule',         // tabla para almacenar reglas
],
```

Agregarlo también a config\console.php
```
'authManager'       => [
	'class'         => 'yii\rbac\DbManager',
	'defaultRoles'  => ['guest'],
],
```
# Migraciones para creación de esquema de la base de datos
Ejecutar las migraciones para crear las tablas

```
./yii migrate
```

Ejecutar el seeder para tener datos de prueba. La configuración se encuentra en commands\SeedController
Se creará el usuario cfcastillol@gmail.com de contraseña 123456 con permisos de admin

```
./yii seed/index
```

Ejecutar la migración para crear las tablas del esquema de roles RBAC

```
./yii migrate --migrationPath=@yii/rbac/migrations
```

Para insertar roles y permisos ejecute el seed RBAC

```
./yii rbac/init
```

Insertar las reglas para que los usuarios solo puedan interactuar con sus propios registros

```
./yii rbac/reglas
```

Se debe crear un registro en auth_item que tenga la regla, por ejemplo
propio-registro y la ruleName = esAutor.

propio registro debe ser el padre del permiso que quiera restringir, por ejemplo
articulo-ver o articulo-actualizar

se debe asignar el permiso propio registro a los usuarios que quiera restringir
en lugar de asignar el permiso ver o eliminar

# Carpetas de guardado de Imágenes

Se deben crear los siguientes directorios con permisos de lectura y escritura

```
/web/img/categorias/*
/web/img/users/*
```

# Extensiones

- Imagine Extension for Yii 2 (https://github.com/yiisoft/yii2-imagine)
```
composer require yiisoft/yii2-imagine
```

- yii2-grid ( https://demos.krajee.com/grid )

```
composer require kartik-v/yii2-grid "dev-master"
```

- AdminLTE Asset Bundle ( https://github.com/dmstr/yii2-adminlte-asset#adminlte-plugins )

```
composer require dmstr/yii2-adminlte-asset "^2.1"
```

- hail812/yii2-adminlte3 ( https://www.yiiframework.com/extension/hail812/yii2-adminlte3 )

```
composer require --prefer-dist hail812/yii2-adminlte3 "*"
```

- yii2-password ( https://demos.krajee.com/password-details/password-input )

```
composer require kartik-v/yii2-password "dev-master"
```

- yii2-widget-datetimepicker ( https://github.com/kartik-v/yii2-widget-datetimepicker )

```
composer require kartik-v/yii2-widget-datetimepicker "*"
```

- Bootstrap DatePicker Widget for Yii2 ( https://github.com/2amigos/yii2-date-picker-widget )

```
composer require 2amigos/yii2-date-picker-widget:~1.0
```

- yii2-mpdf ( https://demos.krajee.com/mpdf )

```
composer require kartik-v/yii2-mpdf "dev-master"
```

- yii2-widget-select2 ( https://github.com/kartik-v/yii2-widget-select2 )

```
composer require kartik-v/yii2-widget-select2 "@dev"
```

- mihaildev/yii2-ckeditor ( https://www.yiiframework.com/extension/mihaildev/yii2-ckeditor )


```
composer require --prefer-dist mihaildev/yii2-ckeditor "*"
```


- yii2-widget-growl ( https://demos.krajee.com/widget-details/growl )

```
composer require kartik-v/yii2-widget-growl "dev-master"
```

- Yii2-widget-fileinput ( https://demos.krajee.com/widget-details/fileinput )

```
composer require kartik-v/yii2-widget-fileinput "dev-master"
```

- yii2-highcharts-widget ( https://www.yiiframework.com/extension/yii2-highcharts-widget )


```
composer require --prefer-dist miloschuman/yii2-highcharts-widget "*"
```

- yii2-widget-datepicker ( https://demos.krajee.com/widget-details/datepicker )


```
composer equire kartik-v/yii2-widget-datepicker "dev-master"
composer require kartik-v/yii2-field-range "*"
```


- yii2-lightbox ( https://github.com/yeesoft/yii2-lightbox )

```
composer require --prefer-dist yeesoft/yii2-lightbox "~0.1.0"
```


-  yii2-icons (https://github.com/kartik-v/yii2-icons)

```
composer require kartik-v/yii2-icons "@dev"
```
